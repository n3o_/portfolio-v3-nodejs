const express = require('express');
const nodemailer = require("nodemailer");
const multiparty = require("multiparty");
const ejs = require('ejs');
require("dotenv").config();
const app = express();
const requestIp = require('request-ip');
const convertToIpv4AndCalc = require('./utils/ipv4-convert-calc');
const displayVisitors = require('./utils/visitors-display');
const count = require('./utils/visitors-count');


const transporter = nodemailer.createTransport({
    host: "smtp-pt.securemail.pro",
    port: 465,
    auth: {
      user: process.env.EMAIL,
      pass: process.env.PASS,
    },
});

  transporter.verify(function (error, success) {
    
  });
app.use(express.static('public'));

app.set('view engine', 'ejs');
 
app.use(requestIp.mw());
app.listen(8080);
app.get('/', (req,res)=>{
    res.render('pages/welcome');
});

app.get('/welcome', async (req,res)=>{
    const ip = req.clientIp;
	console.log(ip);
    await count(convertToIpv4AndCalc(ip));
    let visitorsCount = await displayVisitors();
    res.render('pages/main', {counter: visitorsCount});
});

app.get('/about', async (req,res)=>{
    const ip = req.clientIp;
    await count(convertToIpv4AndCalc(ip));
    let visitorsCount = await displayVisitors();
    res.render('pages/about', {counter: visitorsCount});
});

app.post("/contact", async (req, res) => {
    const ip = req.clientIp;
    await count(convertToIpv4AndCalc(ip));
    let visitorsCount = await displayVisitors();
    let form = new multiparty.Form();
    let data = {};
    form.parse(req, function (err, fields) {
      console.log("Message being sent...");
      Object.keys(fields).forEach(function (property) {
        data[property] = fields[property].toString();
      });
  
      //2. You can configure the object however you want
      const mail = {
        from: data.name,
        to: process.env.EMAIL,
        subject: data.subject,
        text: `${data.name} <${data.email}> \n${data.message}`,
      };
  
      //3.
      transporter.sendMail(mail, (err, data) => {
        if (err) {
          console.log(err);
          res.render('pages/error');
        } else {
            res.render('pages/success', {counter : visitorsCount});
        }
      });
    });
  });
