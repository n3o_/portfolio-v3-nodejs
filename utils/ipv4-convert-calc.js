const pool = require('./mariadb');

const convertToIpv4AndCalc = (ip)=>{
    const ipv4 = ip.split(".");
    const ipv4Converted = (ipv4[0] * 16777216) + (ipv4[1] * 65536 ) + (ipv4[2] * 256) + ipv4[3];
    let date = new Date();
    let hourOfVisit = date.getHours();
    console.log("------------- New visit --------------")
    console.log(ipv4);
    console.log(ipv4Converted);
    console.log(date.getDate() + "/" + (date.getMonth()+1) + " at "+hourOfVisit);
    return ipv4Converted;
};
module.exports = convertToIpv4AndCalc;
