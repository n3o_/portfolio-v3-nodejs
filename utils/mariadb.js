const mariadb = require('mariadb');
const pool = mariadb.createPool({
    host: process.env.MYSQL_HOST, 
    user: process.env.MYSQL_USER,
    database: process.env.MYSQL_DB,
    password: process.env.MYSQL_PASS,
    connectionLimit: 5
});

module.exports = pool;
