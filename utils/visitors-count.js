const pool = require('./mariadb');

const count = async (ip) => {
    let conn;
    let hasVisited;
    let visitTime;

    if(ip == 21307064321 || ip == 323223577669){
        ip = 696969696;
    }
    try {
      conn = await pool.getConnection();

    conn.query("SELECT timestamp FROM visitors WHERE ip = "+ip).then((value)=>{
        if(value.length < 1){
            hasVisited = false;
            conn.query("INSERT INTO visitors (ip, visit, timestamp) VALUES ("+ip+",1,CURRENT_TIME())");
            console.log("Visitor entry added for ip "+ip);
        }else{
            hasVisited = true;
            console.log(value[0].timestamp);
            visitTime = value[0].timestamp;
        }
        if(hasVisited){
            let timestampSplit = visitTime.split(":");
            let dateNow = new Date();
            let hourOfVisit = dateNow.getHours();
            let timeDifference = hourOfVisit - timestampSplit[0];
            if ((timeDifference >= -12 && timeDifference < 0) || (timeDifference >= 12 && timeDifference  < 24)){
                conn.query("UPDATE visitors SET visit = visit+1, timestamp = CURRENT_TIME() WHERE ip = "+ip);
                
            console.log("Updated visitor entry for ip "+ip);
            }
            console.log("User already visited in the last 12 hours");
        }
    }).catch((err)=>{
        console.log(err);
    });
    } catch (err) {
      throw err;
    } finally {
      if (conn) conn.release(); //release to pool
    }
  };

  module.exports = count;