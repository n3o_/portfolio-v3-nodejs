const pool = require('./mariadb');

const displayVisitors = async ()=>{
    let visits = 0;
    let conn;
    try {
      conn = await pool.getConnection();
    return conn.query("SELECT visit FROM visitors").then((values)=>{
        values.forEach((visit)=>{
        visits += visit.visit;
        });
        return visits;
    }).catch(console.error);
    } catch (err) {
      throw err;
    } finally {
      if (conn) conn.release(); //release to pool
    }
};

module.exports = displayVisitors;